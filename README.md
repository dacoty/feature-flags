# Feature-Flags

A sample "feature flags" throwaway application for assisting with my CKAD studies.

## Disclaimer
- This software is covered by the [unlicense](https://choosealicense.com/licenses/unlicense/) 

- I encourage you to download this repo and use/modify offline for whatever your own studies are, since i cannot guarantee this repo will be public and updated forever.
- No warranties of any kind. No support is provided. Please make no MR's to this repo. Just download it as a zip and use however you want. 
- REMEMBER THAT THIS IS A THROWAWAY APP MADE IN VERY LITTLE TIME FOR ME TO HELP WITH MY STUDIES. I hope this is useful to you as well! 

## Using this with Docker:
- Install docker on your computer and run the following commands:
  - `docker build -t feature-flag-system:latest ./app`
  - `docker run -d -p 8080:5000 --name feature-flag-system --link redis:redis feature-flag-system:latest`
- In a web browser, go to `http://localhost:8080` and poke around/try the site out.
- To clean up, run these commands:
  - `docker stop feature-flag-system redis`
  - `docker rm feature-flag-system redis`

## Using this with Kubernetes:
 - In the kubernetes folder, there is an app.yaml file as a starter to get the app up and running.
 - In your kubernetes cluster, run `kubectl apply -f app.yaml`
 - In your web browser, go to `http://localhost:30080` to see the app.
 - Cleanup with `kubectl -delete -f app.yaml` and deleting the docker images using docker rm as shown in the section above.

## Screenshots

![img.png](img.png)

![img_1.png](img_1.png)

![img_2.png](img_2.png)

![img_3.png](img_3.png)


### Kubernetes : https://kubernetes.io/
### Docker: https://www.docker.com/get-started/
### Flask: https://flask.palletsprojects.com/en/3.0.x/
### Redis: https://redis.io/docs/latest/develop/get-started/

