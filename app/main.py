from flask import Flask, request, jsonify, render_template, redirect, url_for, flash
from redis_client import RedisClient

app = Flask(__name__)
app.secret_key = 'supersecretkey'  # Needed for flash messages
redis_client = RedisClient()


@app.route('/set_flag', methods=['POST'])
def set_flag():
    data = request.json
    flag_name = data.get('flag_name')
    value = data.get('value')

    if redis_client.client.exists(flag_name):
        return jsonify({"error": "Flag already exists"}), 400

    redis_client.set_flag(flag_name, value)
    return jsonify({"message": "Flag set successfully"}), 200


@app.route('/get_flag', methods=['GET'])
def get_flag():
    flag_name = request.args.get('flag_name')
    value = redis_client.get_flag(flag_name)
    if value is None:
        return jsonify({"error": "Flag not found"}), 404
    return jsonify({"flag_name": flag_name, "value": value.decode('utf-8')}), 200


@app.route('/')
def index():
    keys = redis_client.client.keys()
    flags = {key.decode('utf-8'): redis_client.get_flag(key).decode('utf-8') for key in keys}
    return render_template('index.html', flags=flags)


@app.route('/toggle/<flag_name>')
def toggle_flag(flag_name):
    current_value = redis_client.get_flag(flag_name).decode('utf-8')
    new_value = 'false' if current_value == 'true' else 'true'
    redis_client.set_flag(flag_name, new_value)
    return redirect(url_for('index'))


@app.route('/add_flag', methods=['POST'])
def add_flag():
    flag_name = request.form['flag_name']
    value = request.form['value']

    if redis_client.client.exists(flag_name):
        flash(f"Flag '{flag_name}' already exists.", "error")
        return redirect(url_for('index'))

    redis_client.set_flag(flag_name, value)
    flash(f"Flag '{flag_name}' added successfully.", "success")
    return redirect(url_for('index'))


@app.route('/delete/<flag_name>')
def delete_flag(flag_name):
    redis_client.client.delete(flag_name)
    flash(f"Flag '{flag_name}' deleted successfully.", "success")
    return redirect(url_for('index'))


@app.route('/edit/<flag_name>', methods=['GET', 'POST'])
def edit_flag(flag_name):
    if request.method == 'POST':
        new_name = request.form['new_flag_name']
        new_value = request.form['value']

        if new_name != flag_name:
            if redis_client.client.exists(new_name):
                flash(f"Flag '{new_name}' already exists.", "error")
                return redirect(url_for('edit_flag', flag_name=flag_name))

            # Rename the flag
            current_value = redis_client.get_flag(flag_name)
            redis_client.client.set(new_name, current_value)
            redis_client.client.delete(flag_name)

        # Update the value
        redis_client.set_flag(new_name, new_value)
        flash(f"Flag '{new_name}' updated successfully.", "success")
        return redirect(url_for('index'))
    else:
        current_value = redis_client.get_flag(flag_name).decode('utf-8')
        return render_template('edit.html', flag_name=flag_name, value=current_value)


if __name__ == '__main__':
    app.run(host='0.0.0.0', port=5000)
