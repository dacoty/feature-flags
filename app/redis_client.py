import redis

class RedisClient:
    def __init__(self, host='redis-service', port=6379, db=0):
        self.client = redis.Redis(host=host, port=port, db=db)

    def set_flag(self, flag_name, value):
        self.client.set(flag_name, value)

    def get_flag(self, flag_name):
        return self.client.get(flag_name)
